server {
        listen 80;
        server_name {{ server_name }};
        root {{ server_root }};

        try_files $uri $uri/index.php;

        error_page 404 /404.html;

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
                root /usr/share/nginx/www;
        }

        location ~ \.php$ {
                fastcgi_split_path_info ^(.+\.php)(/.*)$;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_index index.php;
                send_timeout 1800;
                fastcgi_read_timeout 1800;
                fastcgi_pass unix:/var/run/php5-fpm.sock;
        }

        location ~ /\. {
                deny all;
        }

}
