# Ansible PoC

If you want to provision a Debian 8 ("Jessie") server with Nginx and PHP-FPM, this playbook will help you get started quickly.

## Setup

In `hosts` file you can put your server's IP address. Once done, all you have to do is launch Ansible Playbooks to do the magic for you.

```bash
ansible-playbook -i hosts playbook.yml
```

## Example Vagrant box

For this proof-of-concept I've also created a `Vagrantfile` where all the syncing is disabled and the network is tied to the public network using DHCP.

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "debian/jessie64"
  config.vm.box_check_update = false
  config.vm.host_name = "provision.in2it.be"
  config.vm.network "public_network",
    use_dhcp_assigned_default_route: true,
    bridge: [
      "en0: Wi-Fi (AirPort)",
    ]
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provision "shell", run: "always", inline: "/sbin/ifconfig eth1"
end
```

You might need to adjust the setting for `config.vm.network` so the bridge matches your own settings, but in most cases it will run out of the box.

This will set up your vanilla Debian 8 box, with additional lines giving you information about the IP to connect to.

```bash
    default: eth1      Link encap:Ethernet  HWaddr 08:00:27:aa:95:04
    default:           inet addr:192.168.66.42  Bcast:192.168.66.255  Mask:255.255.255.0
    default:           inet6 addr: fe80::a00:27ff:feaa:9504/64 Scope:Link
    default:           UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
    default:           RX packets:24 errors:0 dropped:0 overruns:0 frame:0
    default:           TX packets:18 errors:0 dropped:0 overruns:0 carrier:0
    default:           collisions:0 txqueuelen:1000
    default:           RX bytes:3694 (3.6 KiB)  TX bytes:3224 (3.1 KiB)
```

In this case, the IP is `192.168.66.42`.

## SSH configuration

If you run the vagrant box on the same environment where you will launch `ansible-playbook`, you can grab the configuration directly from vagrant using `ssh-config`.

```bash
vagrant ssh-config >> ~/.ssh/config
```

Make sure you change the `Host default` to `Host <ip-address>` in your `~/.ssh/config` otherwise ansible is not able to "find" your server.


